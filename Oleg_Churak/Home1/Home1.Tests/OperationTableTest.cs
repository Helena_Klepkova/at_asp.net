﻿using System;
using NUnit.Framework;
using Home1;
using Moq;
using System.Collections.Generic;

namespace Home1.Tests
{
    [TestFixture]
    public class OperationTableTest
    {
        private OperationTable calc;

        // will be invoked before each test method
        [SetUp]
        public void SetUp()
        {
            calc = new OperationTable();
        }


        [Test]
        public void SumTest_3plus2_returns5()
        {
            // arrange
            int a = 3;
            int b = 2;
            int expected = 5;

            //act
            var actual = calc.Sum(a, b);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void SubstractTest_5minus2_returns3()
        {
            int a = 5;
            int b = 2;
            int expected = 3;

            var actual = calc.Sub(a, b);

            Assert.AreEqual(expected, actual);
        }

        [TestCase(8, 4, 32)]
        [TestCase(3, -8, -24)]
        [TestCase(0, 3, 0)]
        public void MultiplyTest(int x, int y, int expected)
        {
            var actual = calc.Multiply(x, y);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void AverageTest_55div5_returns11()
        {
            var mock = new Mock<IHandlingList>();
            mock.Setup(item => item.GetListOfInts(5)).Returns(new List<int>() { 9, 10, 11, 12, 13 });
            int expected = 11;

            float actual = calc.Average(mock.Object);

            Assert.AreEqual(expected, actual);
        }
    }
}
