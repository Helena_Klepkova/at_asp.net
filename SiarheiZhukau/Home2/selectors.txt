На странице google.com выписать следующие CSS и XPATH селекторы:
1. Строка для ввода кейворда для поиска
CSS: input#lst-ib
XPATH: //input[@id="lst-ib"]
2. Кнопка "Поиск в Google"
CSS: input[name="btnK"]                                                                                
XPATH: //input[@name="btnK"]
3. Кнопка "Мне повезет"
CSS: input[name="btnI"]
XPATH: //input[@name="btnI"]

На странице результатов гугла:
1. Ссылки на результаты поиска (все)
CSS: .r>a
XPATH: //*[@class="r"]/a
2. 5-я буква о в Goooooooooogle (внизу, где пагинация)
CSS: #nav td:nth-child(6) span
XPATH: //*[@id="nav"]//td[6]//span

На странице yandex.com:
1. Login input field
CSS: input[name="login"]
XPATH: //input[@name="login"]
2. Password input field
CSS: input[name="passwd"]
XPATH: //input[@name="passwd"]
3. "Enter" button in login form
CSS: .domik2__submit button
XPATH: //*[@class="domik2__submit"]/button

На странице яндекс почты (у кого нет ящика - зарегистрируйте)
1. Ссылка "Входящие"
CSS: *[data-fid="1"]
XPATH: //*[@data-fid="1"]
2. Ссылка "Исходящие"
CSS: *[data-fid="4"]
XPATH: //*[@data-fid="4"]
3. Ссылка "Спам"
CSS: *[data-fid="2"]
XPATH: //*[@data-fid="2"]
4. Ссылка "Удаленные"
CSS: *[data-fid="3"]
XPATH: //*[@data-fid="3"]
5. Ссылка "Черновики"
CSS: *[data-fid="6"]
XPATH: //*[@data-fid="6"]
6. Кнопка "Новое письмо"
CSS: div.ns-view-toolbar a[href="#compose"]
XPATH: //*[contains(@class,"ns-view-toolbar")]//a[@href="#compose"]
7. Кнопка "Обновить"
CSS: div.ns-view-toolbar [data-click-action="mailbox.check"]
XPATH: //*[contains(@class,"ns-view-toolbar")]//*[@data-click-action="mailbox.check"]
8. Кнопка "Отправить" (на странице нового письма)
CSS: #nb-17
XPATH: //*[@id="nb-17"]
9. Кнопка "Пометить как спам"
CSS: .ns-view-toolbar-button-spam
XPATH: //*[contains(@class,"ns-view-toolbar-button-spam")]
10. Кнопка "Пометить прочитанным"
CSS: .ns-view-toolbar-button-mark-as-read
XPATH: //*[contains(@class,"ns-view-toolbar-button-mark-as-read")]
11. Кнопка "Переместить в другую директорию"
CSS: .ns-view-toolbar-button-folders-actions
XPATH: //*[contains(@class,"ns-view-toolbar-button-folders-actions")]
12. Кнопка "Закрепить письмо"
CSS: .ns-view-toolbar-button-pin
XPATH: //*[contains(@class,"ns-view-toolbar-button-pin")]
13. Селектор для поиска уникального письма
CSS: ._nb-input-content input
XPATH: //*[contains(@class,"_nb-input-content input")]

На странице яндекс диска
1. Кнопка загрузить файлы
CSS: .button__attach
xpath: //*[@class="button__attach"]
2. Селектор для уникального файла на диске
CSS: *[data-id="/disk/App_Data.rar"]
xpath: //*[@data-id="/disk/App_Data.rar"]
3. Кнопка скачать файл                          
CSS: button[data-click-action="resource.download"]
xpath: //button[@data-click-action="resource.download"]
4. Кнопка удалить файл
CSS: button[data-click-action="resource.delete"]
xpath: //button[@data-click-action="resource.delete"]
5. Кнопка в корзину
CSS: button[data-click-action="resource.delete"]
xpath: //button[@data-click-action="resource.delete"]
6. Кнопка восстановить файл
CSS: button[data-click-action="resource.restore"]
xpath: //button[@data-click-action="resource.restore"]
