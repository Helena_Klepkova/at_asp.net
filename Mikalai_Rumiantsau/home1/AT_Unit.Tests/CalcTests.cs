﻿using NUnit.Framework;
//
using AT_Unit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT_Unit.Tests
{
    [TestFixture]
    public class CalcTests
    {

        private Calc calc;

        // will be invoked before each test method
        [SetUp]
        public void SetUp()
        {
            calc = new Calc();
        }


        [Test]
        public void SumTest_3plus2_returns5()
        {
            // arrange
            int a = 3;
            int b = 2;
            int expected = 5;
            //act
            var actual = calc.Sum(a, b);
            //assert
            Assert.AreEqual(expected, actual);
        }


        // this test should fail, code has a mistake
        [Test]
        public void SubstractTest_5minus2_returns3()
        {
            int a = 5;
            int b = 2;
            int expected = 3;
            var actual = calc.Substract(a, b);
            Assert.AreEqual(expected, actual);
        }


        [Test]
        public void DivideTest_5divideTo0_throwsArgumentException()
        {
            double a = 5;
            double b = 0;

            Assert.Throws<ArgumentException>(() => calc.Divide(a, b));
        }


        [Test]
        public void DivideTest_4divideTo2_returnsValueTypeOfDouble()
        {
            double a = 4;
            double b = 2;

            var result = calc.Divide(a, b);

            Assert.IsInstanceOf<double>(result);
        }

        [TestCase(2, 3, 6)]
        [TestCase(3, 3, 9)]
        [TestCase(0, 3, 0)]
        public void MultipleTest(int a, int b, int expected)
        {
            var actual = calc.Multiple(a, b);
            Assert.AreEqual(expected, actual);
        }

        // will be invoked after each test method
        [TearDown]
        public void TearDown()
        {
            // unnecessary string, just for example
            calc = null;
        }

    }
}