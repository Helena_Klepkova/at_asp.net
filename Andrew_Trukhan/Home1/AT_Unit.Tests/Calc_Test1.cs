﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AT_Unit;

namespace AT_Unit.Tests
{
    [TestClass]
    public class CalcTest
    {
        private Calc calc;

        [TestInitialize]
        public void NewCalc()
        {
         calc = new Calc();
        }

        [TestMethod]
        public void SumTest_2plus8_returns10()
        {
            int a = 2;
            int b = 8;
            int expected = 10;

            var actual = calc.Sum(a, b);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void SubstractTest_6minus3_returns3()
        {
            int a = 6;
            int b = 3;
            int expected = 2;

            var actual = calc.Substract(a, b);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void DivideTest_6div3_returns2()
        {
            int a = 6;
            int b = 3;
            int expected = 2;

            var actual = calc.Divide(a, b);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void DivideTest_6div0_returnsthrowsArgumentException()
        {
            int a = 6;
            int b = 0;
            var actual = calc.Divide(a, b);           
        }

        [TestMethod]
        public void DivideTest_8divideTo2_returnsValueTypeOfDouble()
        {
            double a = 4;
            double b = 2;

            var result = calc.Divide(a, b);

            Assert.IsInstanceOfType(result, typeof(double));
        }

        [TestMethod]
        public void MultipleTest_3multiply2_returns6()
        {
            int a = 3;
            int b = 2;
            int expected = 6;
            var actual = calc.Multiple(a, b);

            Assert.AreEqual(expected, actual);
        }
        
        [TestMethod]
        public void MultipleTest_3multiply0_returns0()
        {
            int a = 3;
            int b = 0;
            int expected = 0;
            var actual = calc.Multiple(a, b);

            Assert.AreEqual(expected, actual);
        }

        [TestCleanup]
        public void Clean()
        {
            calc = null;
        }

    }
}
