﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT_Unit
{
    public class Repo : IRepo
    {
        public List<int> NextInts()
        {
            Random rnd = new Random();

            List<int> numbers = new List<int>();
            numbers.Add(rnd.Next());
            numbers.Add(rnd.Next());

            return numbers;
        }
    }
}
