﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home1
{
	public enum DirectionOfRotation
	{
		Left,
		Right,
		None
	}

	public interface IMotor
	{
		int Speed { get; }
		DirectionOfRotation CurrentDirection { get; }

		void Start(DirectionOfRotation direction);
		void Stop();
		void SpeedUp(int speedIncrement);
	}
}
