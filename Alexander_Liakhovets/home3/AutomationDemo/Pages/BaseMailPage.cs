﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutomationDemo.Framework;
using OpenQA.Selenium;

namespace AutomationDemo.Pages
{
	public class BaseMailPage
	{
		internal Browser browser = Browser.Instance;

		public const string MailSubject = "AutoTestMessage";

		internal static readonly By testMailMessageByXPath = By.XPath(string.Format("//span[@title='{0}']//ancestor::a//label", MailSubject));
		internal static readonly By inboxMailsLinkByCss = By.CssSelector("div[data-key='view=folders'] a[href='#inbox']");
		internal static readonly By sentMailsLinkByCss = By.CssSelector("div.ns-view-folders a[href='#sent']");
		internal static readonly By deletedMailsLinkByCss = By.CssSelector("div.ns-view-folders a[href='#trash']");
		internal static readonly By creatrNewMailButtonByCss = By.CssSelector("a[href='#compose']");
		internal static readonly By refreshMailListButtonByCss = By.CssSelector("div[data-click-action='mailbox.check']");
		internal static readonly By deliteMailButtonByCss = By.CssSelector("div.ns-view-toolbar-button-delete");
		internal static readonly By selectAllCheckBoxByCss = By.CssSelector("label.mail-Toolbar-Item-Checkbox");
		internal static readonly By userProfileLabelByCss = By.CssSelector(".ns-view-head-user .mail-User-Name");


		public IWebElement InboxMailsLink { get { return browser.FindElement(inboxMailsLinkByCss); } }
		public IWebElement SentMailsLink { get { return browser.FindElement(sentMailsLinkByCss); } }
		public IWebElement DeletedMailsLink { get { return browser.FindElement(deletedMailsLinkByCss); } }
		public IWebElement CreateNewMailButton { get { return browser.FindElement(creatrNewMailButtonByCss); } }
		public IWebElement RefreshMailLIstButton { get { return browser.FindElement(refreshMailListButtonByCss); } }
		public IWebElement DeliteMailButton { get { return browser.FindElement(deliteMailButtonByCss); } }
		public IWebElement TestMailMessage { get { return browser.FindElement(testMailMessageByXPath); } }
		public IWebElement SelectAllCheckBox { get { return browser.FindElement(selectAllCheckBoxByCss); } }
		public IWebElement UserProfileLabel { get { return browser.FindElement(userProfileLabelByCss); } }



		public void RefreshPage()
		{
			browser.Refresh();
		}

		public bool UserIsLogin()
		{
			bool result;
			try
			{
				result = UserProfileLabel.Displayed;
			}
			catch (Exception)
			{
				result = false;
			}
			return result;
		}

		public BaseMailPage SelectAllMails()
		{
			SelectAllCheckBox.Click();
			return new BaseMailPage();
		}

		public NewMailPage CreateNewMail()
		{
			CreateNewMailButton.Click();
			return new NewMailPage();
		}

		public bool FindTestMessge()
		{
			bool result;
			try
			{
				Thread.Sleep(5000);
				RefreshMailLIstButton.Click();
				Thread.Sleep(3000);
				result = TestMailMessage.Displayed;
			}
			catch (Exception)
			{

				result = false;
			}
			return result;
		}

		public BaseMailPage DeleteTestMessage()
		{
			RefreshMailLIstButton.Click();
			Thread.Sleep(1000);
			TestMailMessage.Click();
			DeliteMailButton.Click();
			return new BaseMailPage();
		}

		public BaseMailPage PermanentDeleteTestMessage()
		{
			DeliteMailButton.Click();
			return new BaseMailPage();
		}

		public InboxMailPage GoToInboxPage()
		{
			Thread.Sleep(1000);
			InboxMailsLink.Click();
			RefreshPage();
			return new InboxMailPage();
		}

		public SentMailPage GoToSentPage()
		{
			Thread.Sleep(1000);
			SentMailsLink.Click();
			RefreshPage();
			return new SentMailPage();
		}

		public TrashMailPage GotoTrashPage()
		{
			Thread.Sleep(1000);
			DeletedMailsLink.Click();
			RefreshPage();
			return new TrashMailPage();
		}

	}
}
