google.com:
	1.search_line:
		xpath: //input[@id='lst-ib']
		css: input#lst-ib
	2.search_button:
		xpath: //input[@name='btnK']
		css: input[name="btnK"]
	3.lucky_button:
		xpath: //input[@name='btnI']
		css: input[name='btnI']

google.com_by_search_results:
	1.all_links_by_search:
		xpath: //h3[@class='r']/a/@href
		css: h3.r>a[href*="https"]
	2.five_letter_O:
		xpath: //a[@aria-label='Page 5']/span
		css: a[aria-label='Page 5'] span

yandex.com:
	1.Login_input_field:
		xpath: //input[@name='login']
		css: input[name='login']
	2.Password_input_field:
		xpath: //input[@name='passwd']
		css: input[name='passwd']
	3.Enter_button_in_login_form:
		xpath: //form//button[contains(@class,'nb-group-start')]
		css: form button.nb-group-start

mail.yandex.com:
	1.href_inbox:
		xpath: //a[contains(@class,'ns-view-folder')][@href='#inbox']
		css: a.ns-view-folder[href='#inbox']
	2.href_outbox:
		xpath: //a[contains(@class,'ns-view-folder')][@href='#sent']
		css: a.ns-view-folder[href='#sent']
	3.href_spam:
		xpath: //a[contains(@class,'ns-view-folder')][@href='#spam']
		css: a.ns-view-folder[href='#spam']
	4.href_deleted:
		xpath: //a[contains(@class,'ns-view-folder')][@href='#trash']
		css: a.ns-view-folder[href='#trash']
	5.href_draft:
		xpath: //a[contains(@class,'ns-view-folder')][@href='#draft']
		css: a.ns-view-folder[href='#draft']
	6.button_new_message:
		xpath: //div[contains(@class,'ns-view-toolbar')]//a[@href='#compose']
		css: div.ns-view-toolbar a[href='#compose']
	7.button_refresh:
		xpath: //div[contains(@class,'ns-view-toolbar')][@data-click-action='mailbox.check']
		css: div.ns-view-toolbar [data-click-action='mailbox.check']
	8.button_send_message:
		xpath: //div[contains(@class, 'mail-Compose-Message')]//button[@type='submit']
		css: div.mail-Compose-Message button[type='submit']
	9.button_marking_spam:
		xpath://div[contains(@class, 'ns-view-toolbar-button-spam')]
		css: div.ns-view-toolbar-button-spam
	10.button_marking_as_read:
		xpath: //div[contains(@class, 'ns-view-toolbar-button-mark-as-read')]
		css: div.ns-view-toolbar-button-mark-as-read
	11.button_transfer_letter_in_another_folder:
		xpath: //div[contains(@class,'ns-view-toolbar-button-folders-actions')]
		css: div.ns-view-toolbar-button-folders-actions
	12.button_secure_message:
		xpath: //div[contains(@class,'ns-view-toolbar-button-pin')]
		css: div.ns-view-toolbar-button-pin
	13.selector_for_search_of_unique_message:
		xpath: //label[contains(@class, 'mail-Search-Input')]//input
		css: label.mail-Search-Input input

disc.yandex.com:
	1.button_upload_files:
		xpath: //input[@class='button__attach']
		css: input.button__attach
	2.selector_for_finding_file_on_screen:
		xpath: //div[contains(@class, 'nb-resource')][contains(@title, 'Build-and-Program-Your')]
		css: div.nb-resource[title*='Build-and-Program-Your']
	3.button_download_files:
		xpath: //button[@data-click-action='resource.download']
		css: button[data-click-action='resource.download']
	4.button_delete_file:
		xpath: //button[@data-click-action='resource.delete']
		css: button[data-click-action='resource.delete']
	5.button_in_trash:
		xpath: //a[contains(@class,'navigation__item')][@href='/client/trash']
		css: a.navigation__item[href='/client/trash']
	6.button_restore_file:
		xpath: //button[@data-click-action='resource.restore']
		css: button[data-click-action='resource.restore']