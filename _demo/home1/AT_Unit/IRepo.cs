﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT_Unit
{
    public interface IRepo
    {
        List<int> NextInts();
    }
}
