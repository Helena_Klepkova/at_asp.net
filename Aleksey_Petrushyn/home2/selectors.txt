Google.com:
1. Строка ввода кейворда для поиска
    XPATH:  //input[@id='lst-ib'] (где угодно глубоко выбираем input с id='lst-ib')
    CSS:    div #lst-ib
2. Кнопка "Поиск в Google"
    XPATH:  //input[@value='Google Search']
    CSS:    [name=btnK]
3. Кнопка "Мне повезет"
    XPATH:  //input[@value='I'm Feeling Lucky']
    CSS:    [name=btnI]

На странице результатов гугла:
1. Ссылки на результаты поиска (все)
    XPATH:  //div[@class='g']//h3[@class='r']/a
    CSS:    .r a
2. 5-я буква о в Goooooooooogle (внизу, где пагинация)
    XPATH:  //a[@aria-label='Page 5']/span
    CSS:    a[aria-label='Page 5'] span

На странице yandex.com:
1. Login input field
    XPATH:  //input[@name='login']
    CSS:    input[name=login]
2. Password input field
    XPATH:  //input[@name='passwd']
    CSS:    input[name=passwd]
3. "Enter" button in login form
    XPATH:  //div[@class='domik2__submit']/button
    CSS:    div[class=domik2__submit] button

На странице яндекс почты:
1. Ссылка "Входящие"
    XPATH:  //div[@class='mail-Layout-Inner']//a[@href='#inbox']
    CSS:    div[class='ns-view-left-box ns-view-id-6 mail-Layout-Aside-Inner-Box'] a[href='#inbox']
2. Ссылка "Исходящие"
    XPATH:  //div[@class='mail-Layout-Inner']//a[@href='#sent']
    CSS:    div[class='ns-view-left-box ns-view-id-6 mail-Layout-Aside-Inner-Box'] a[href='#sent']
3. Ссылка "Спам"
    XPATH:  //div[@class='mail-Layout-Inner']//a[@href='#spam']
    CSS:    div[class='ns-view-left-box ns-view-id-6 mail-Layout-Aside-Inner-Box'] a[href='#spam']
4. Ссылка "Удаленные"
    XPATH:  //div[@class='mail-Layout-Inner']//a[@href='#trash']
    CSS:    div[class='ns-view-left-box ns-view-id-6 mail-Layout-Aside-Inner-Box'] a[href='#trash']
5. Ссылка "Черновики"
    XPATH:  //div[@class='mail-Layout-Inner']//a[@href='#draft']
    CSS:    div[class='ns-view-left-box ns-view-id-6 mail-Layout-Aside-Inner-Box'] a[href='#draft']
6. Кнопка "Новое письмо"
    XPATH:  //a[@href='#compose']
    CSS:    a[href='#compose']
7. Кнопка "Обновить"
    XPATH:  //div[@data-click-action='mailbox.check']
    CSS:    div[data-click-action='mailbox.check']
8. Кнопка "Отправить" (на странице нового письма)
    XPATH:  //button[@type='submit']
    CSS:    button[type=submit]
9. Кнопка "Пометить как спам"
    XPATH:  //div[@title="Это спам! (Shift + s)"]
    CSS:    div[title='Это спам! (Shift + s)']
10. Кнопка "Пометить прочитанным"
    XPATH:  //div[@title="Прочитано (q)"]
    CSS:    div[title='Прочитано (q)']
11. Кнопка "Переместить в другую директорию"
    XPATH:  //div[@title="В папку (m)"]
    CSS:    div[title='В папку (m)']
12. Кнопка "Закрепить письмо"
    XPATH:  //div[@title="Закрепить"]
    CSS:    div[title='Закрепить']
13. Селектор для поиска уникального письма
    XPATH:  //span[@title='UNIQ']
    CSS:    span[title=UNIQ]

На странице яндекс диска
1. Кнопка загрузить файлы
    XPATH:  //span[@title='Загрузить файлы']
    CSS:    span[title='Загрузить файлы']
2. Селектор для уникального файла на диске
    XPATH:  //div[@data-id='/disk/UNIQ']
    CSS:    div[data-id='/disk/UNIQ']
3. Кнопка скачать файл
    XPATH:  //button[@title='Скачать']
    CSS:    button[title=Скачать]
4. Кнопка удалить файл
    XPATH:  //button[@title='Удалить']
    CSS:    button[title=Удалить]
5. Кнопка в корзину
    XPATH:  //div[@data-id='/trash']
    CSS:    div[data-id='/trash']
6. Кнопка восстановить файл
    XPATH:  //div[@class="nb-panel nb-panel_aside"]//button[1]
    CSS:    div[class='nb-panel nb-panel_aside'] button:first-child