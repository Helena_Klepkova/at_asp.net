﻿using NUnit.Framework;
using QA_Task0_Calculator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;

namespace QA_Task0_Calculator.Tests
{
	[TestFixture()]
	public class CalculatorTests
	{
		private Calculator calculator;

		[SetUp]
		public void SetUp()
		{
			calculator = new Calculator();
		}

		[Test()]
		public void SumTest_5plus7_return12()
		{
			//arrange
			int a = 5;
			int b = 7;
			int expected = 12;
			//action
			int actual = calculator.GetSum(a, b);
			//assert
			Assert.AreEqual(expected, actual);
		}

		[Test()]
		public void SubTest_21min6_return15()
		{
			//arrange
			int a = 21;
			int b = 6;
			int expected = 15;
			//action
			int actual = calculator.GetSubtraction(a, b);
			Assert.AreEqual(expected, actual);
		}


		[TestCase(20, 5, 100)]
		[TestCase(6, 15, 90)]
		[TestCase(54, 0, 0)]
		public void MultTest(int a, int b, int expected)
		{
			int actual = calculator.GetMultiplication(a, b);
			Assert.AreEqual(expected, actual);
		}

		[TestCase(50, 5, 10)]
		[TestCase(45, 5, 9)]

		public void DivTest(int a, int b, double expected)
		{
			double actual = calculator.GetDivision(a, b);
			Assert.AreEqual(expected, actual);
		}

		[Test]
		public void DivZeroTest()
		{
			int a = 12;
			int b = 0;
			Assert.Throws<ArgumentException>(() => calculator.GetDivision(a, b));
		}

		[TearDown]
		public void TearDown()
		{
			calculator = null;
		}
	}
}