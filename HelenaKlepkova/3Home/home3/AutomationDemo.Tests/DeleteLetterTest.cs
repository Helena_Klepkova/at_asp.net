﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using AutomationDemo.Services;
using AutomationDemo.Services.Models;

namespace AutomationDemo.Tests
{
    [TestFixture]
    class DeleteLetterTest : BaseTest
    {
        LoginService loginService = new LoginService();
        DeleteLetterService deleteLetterService = new DeleteLetterService();
        OptionMessageService optionMessageService = new OptionMessageService();

        [Test]
        public void DeleteLetter_Test()
        {
            loginService.LoginToMailBox(AccountFactory.Account);
            deleteLetterService.DeleteLetter();
            Assert.IsFalse(optionMessageService.UniqueLetterFind());
        }

        [Test]
        public void PermanentRemovalOfLetter()
        {
            loginService.LoginToMailBox(AccountFactory.Account);
            deleteLetterService.DeleteLetterFromTrash();
            Assert.IsFalse(optionMessageService.UniqueLetterFind());

        }
    }
}
