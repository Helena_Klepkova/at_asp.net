﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutomationDemo.Services.Models;
using AutomationDemo.Pages;
using OpenQA.Selenium;
using AutomationDemo.Framework;

namespace AutomationDemo.Services
{
    public class OptionMessageService
    {
        LoginSuccessPage loginSuccessPage = new LoginSuccessPage();
        string message = "some text";
        public string Message
        {
            get { return message; }
        }


        public void SendNewMessage_andReturnInbox()
        {
            loginSuccessPage.NewMessageButton.Click();
            loginSuccessPage.Destination.SendKeys(AccountFactory.Account.Email);
            loginSuccessPage.MessageBox.SendKeys(Message);
            Thread.Sleep(1000);
            loginSuccessPage.SendButton.Click();
            Thread.Sleep(1000);
            loginSuccessPage.InboxLink.Click();
        }

        public void OpenSentMessage()
        {
            loginSuccessPage.SentLink.Click();
        }

        public bool UniqueLetterFind()
        {
            bool result;
            try
            {
                if (loginSuccessPage.UniqueLetter != null)
                {
                    result = true;
                }
                else result = false;
            }
            catch (NoSuchElementException)
            {
                result = false;
            }

            return result;
        }
    }
}
