﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationDemo.Services.Models;
using AutomationDemo.Pages;
using OpenQA.Selenium;
using AutomationDemo.Framework;

namespace AutomationDemo.Services
{
    public class DeleteLetterService
    {
        LoginSuccessPage loginSuccessPage = new LoginSuccessPage();
        LetterPage letterPage = new LetterPage();

        public void DeleteLetter()
        {
            try
            {
                IWebElement letter = loginSuccessPage.UniqueLetter;
                letter.Click();
                letterPage.DeleteLetterButton.Click();
            }
            catch (NoSuchElementException) 
            {
                return;
            }         
        }

        public void DeleteLetterFromTrash()
        {
            loginSuccessPage.TrashLink.Click();
            try
            {
                IWebElement letter = loginSuccessPage.UniqueLetter;
                letter.Click();
                letterPage.DeleteLetterButton.Click();
            }
            catch (NoSuchElementException)
            {
                return;
            }         
        }
    }
}
