
Строка_для_ввода_кейворда_для_поиска  
                                XPATH: //input[@id="lst-ib"]
                                  CSS: input[id="lst-ib"]

Кнопка_Поиск_в_Google  
                       XPATH: //input[@value="Поиск в Google"]
                         CSS: input[value="Поиск в Google"]

Кнопка_Мне_повезет  
                       XPATH: //input[@value="Мне повезёт!"]
                         CSS: input[value="Мне повезёт!"]



Ссылки_на_результаты_поиска_все  
                         XPATH: //div[@id="rso"]/div[@class="srg"]/div[@class="g"]/div[@class="rc"]/h3[@class="r"]/a
                           CSS: div#rso div.srg div.g div.rc h3.r a

5-я_буква_о_в_Goooooooooogle  
                         XPATH: //table[@id="nav"]/ tbody/tr/td[6]/a/span 
                           CSS: table#nav td:nth-child(6) a span



Login_input_field  
               XPATH: //input[@name="login"]
                 CSS: input[name="login"]

Password_input_field  
               XPATH: //input[@name="passwd"]
                 CSS: input[name="passwd"]

Enter_button_in_login_form
               XPATH: //div[@class="domik2__submit"]/button[@type="submit"]
                 CSS: div.domik2__submit > button[type="submit"]



Ссылка_Входящие  
            XPATH: //a[@href="#inbox"]
              CSS: a[href="#inbox"]

Ссылка_Исходящие  
            XPATH: //a[@href="#sent"]
              CSS: a[href="#sent"]

Ссылка_Спам  
            XPATH: //a[@href="#spam"]
              CSS: a[href="#spam"]

Ссылка_Удаленные
            XPATH: //a[@href="#trash"]
              CSS: a[href="#trash"]

Ссылка_Черновики
            XPATH: //a[@href="#draft"]
              CSS: a[href="#draft"]

Кнопка_Новое_письмо
            XPATH: //a[@href="#compose"]
              CSS: a[href="#compose"]

Кнопка_Обновить
            XPATH: //div[@title="Проверить, есть ли новые письма (F9)"]
              CSS: div[title="Проверить, есть ли новые письма (F9)"]

Кнопка_Отправить
            XPATH: //button[@title="Отправить письмо (Ctrl + Enter)"]
              CSS: button[title="Отправить письмо (Ctrl + Enter)"]

Кнопка_Пометить_как_спам  
            XPATH: //div[@title="Это спам! (Shift + s)"]
              CSS: div[title="Это спам! (Shift + s)"]

Кнопка_Пометить_прочитанным
            XPATH: /div[@title="Прочитано (q)"]
              CSS: div[title="Прочитано (q)"]

Кнопка_Переместить_в_другую_директорию
            XPATH: //div[@title="В папку (m)"]
              CSS: div[title="В папку (m)"]

Кнопка_Закрепить_письмо
            XPATH: //div[@title="Закрепить"]
              CSS: div[title="Закрепить"]

Селектор_для_поиска_уникального_письма
            XPATH: //div[@class="mail-MessageSnippet-Content"]//span[@title="Please verify your email address"]  // replace "Please verify your email address"  with your letter subject
              CSS: div[class="mail-MessageSnippet-Content"] span[title="Please verify your email address"]  



Кнопка_загрузить_файлы  
                 XPATH: //input[@class="button__attach"]
                   CSS: input[class="button__attach"]

Селектор_для_уникального_файла_на_диске  
                 XPATH: //div[class="recent-files__content"]/div/div/div[@title="ataka.avi"]          // replace "ataka.avi" with your filename
                   CSS: div[class="recent-files__content"] > div > div >div[title="ataka.avi"]

Кнопка_скачать_файл  
                 XPATH: //button[@title="Скачать"]
                   CSS: button[title="Скачать"]

Кнопка_удалить_файл  
                 XPATH: //button[@title="Удалить"]
                   CSS: button[title="Удалить"]

Кнопка_в_корзину  
                 XPATH: xpath_selector      //  the same as Кнопка_удалить_файл
                   CSS: css_selector

Кнопка_восстановить_файл
                 XPATH: //button[@data-click-action="resource.restore"]
                   CSS: button[data-click-action="resource.restore"]
